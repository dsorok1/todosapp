package edu.towson.cosc431.Soroka.todos.models

import com.google.gson.Gson

class Todo constructor (var id: String, var title : String, var contents : String,
                        var isCompleted : Boolean, var image : String,
                        var dateCreated : String) {

    //Uses Gson to serialize the song into json
    fun toJson(): String {
        return Gson().toJson(this)
    }

    override fun toString(): String {
        val str = "Todo: " + title + " | " +
                "Description: " + contents + " | " +
                "Is Completed: " + isCompleted.toString() + " | " +
                "Image: " + image + " | " +
                "Date Created: " + dateCreated
        return str
    }

    fun toggleCompleted()
    {
        isCompleted = !isCompleted
    }
}