package edu.towson.cosc431.Soroka.todos.interfaces

import edu.towson.cosc431.Soroka.todos.models.Todo

interface ITodoRepository {
    fun toJson(): String
    fun getCount(): Int
    fun getTodo(idx: Int): Todo
    fun getAll(): List<Todo>
    fun remove(todo: Todo)
    fun replace(idx: Int, todo: Todo)
    fun addTodo(todo: Todo)
    fun toggleCompleted(idx: Int)
    fun getIndexOf(todo : Todo): Int
}