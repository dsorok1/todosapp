/*
TODO: I think the date is not how the professor wants it to be, but it technically didn't specify how to do that
TODO: fix some sloppy design when it comes to passing things through intents (editing... etc)

*/
package edu.towson.cosc431.Soroka.todos

import android.app.Activity
import android.content.Intent
import android.graphics.PorterDuff
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.widget.Toast
import com.google.gson.Gson
import edu.towson.cosc431.Soroka.todos.fragments.TodoFilterFragment
import edu.towson.cosc431.Soroka.todos.interfaces.ITodoController
import edu.towson.cosc431.Soroka.todos.interfaces.ITodoRepository
import edu.towson.cosc431.Soroka.todos.models.Todo
import edu.towson.cosc431.Soroka.todos.models.TodoRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.todo_filter_fragment.*

//TODO: fix all the errors that are just happening for some reason

class MainActivity : AppCompatActivity(), ITodoController {

    var lastIndexToReplace = -1
    var currentFilter :Filter = Filter.ALL

    lateinit var todos: ITodoRepository
    lateinit var displayedTodos: List<Todo>

    lateinit var frag: TodoFilterFragment

    override fun deleteTodo(todoIndex: Int) {
        //val todo = todos.getTodo(todoIndex)
        val todo = displayedTodos[todoIndex]
        todos.remove(todo)
        updateDisplay()
    }

    override fun toggleCompleted(todoIndex: Int) {
        //todos.toggleCompleted(todoIndex)
        val todo = displayedTodos[todoIndex]
        todos.toggleCompleted(todos.getIndexOf(todo))
        updateDisplay()
    }

    override fun addTodo(todo: Todo) {
        todos.addTodo(todo)
        db.addTodo(todo)
        updateDisplay()
    }

    override fun editTodo(todoIndex: Int){
        //val editingTodo = todos.getTodo(todoIndex)
        val editingTodo = displayedTodos[todoIndex]

        val intent = Intent(this, NewTodoActivity::class.java)


        intent.putExtra(TAG_INDEX_TO_REPLACE, todoIndex)
        //above doesn't work, here's a quick fix
        //lastIndexToReplace = todoIndex
        lastIndexToReplace = todos.getIndexOf(editingTodo)

        intent.putExtra(NewTodoActivity.TAG_TITLE, editingTodo.title)
        intent.putExtra(NewTodoActivity.TAG_CONTENTS, editingTodo.contents)
        intent.putExtra(NewTodoActivity.TAG_COMPLETED, editingTodo.isCompleted)
        intent.putExtra(NewTodoActivity.TAG_IMAGE, editingTodo.image)

        startActivityForResult(intent, EDIT_TODO_CODE)

        //go to NewTodoActivity

        //updateDisplay()

        //todos.replace(todoIndex, )
    }

    override fun launchAddTodoScreen() {
        val intent = Intent(this, NewTodoActivity::class.java)
        startActivityForResult(intent, ADD_TODO_CODE)
    }


    fun setFilter(f: Filter): List<Todo>
    {
        val todoList = ArrayList<Todo>()

        var itr = 0
        while(itr < todos.getAll().size)
        {
            if(f == Filter.ACTIVE)
            {
                Log.d("btnActive", "clicked")
                if (!(todos.getAll()[itr].isCompleted))
                {//not completed
                    todoList.add(todos.getAll()[itr])
                }
            }
            else if(f == Filter.COMPLETED)
            {
                Log.d("btnCompleted", "clicked")
                if (todos.getAll()[itr].isCompleted)
                {//is completed
                    todoList.add(todos.getAll()[itr])
                }
            }
            else if(f == Filter.ALL)
            {
                Log.d("btnAll", "clicked")
                todoList.add(todos.getAll()[itr])
            }

            itr++
        }

        return todoList
    }
    fun updateCurrentFilter(f: Filter)
    {
        currentFilter = f
    }
    fun updateDisplay()
    {
        displayedTodos = setFilter(currentFilter)
        displayTodos(displayedTodos)
    }
    fun displayTodos(tdl: List<Todo>)
    {
        displayedTodos = tdl
        val adapter = TodoAdapter(tdl, this, this)
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this)
    }


    private lateinit var db: TodoDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //create database
        db = TodoDatabase(this)

        if(savedInstanceState == null) {
            //only have to do this stuff once, if coming from a new open
            todos = TodoRepository(db)
            displayedTodos = todos.getAll()

        }
        else
        {
            //if coming from a rotation
            val todosJson = savedInstanceState.getString("todos")
            todos = Gson().fromJson(todosJson, TodoRepository::class.java)
            displayedTodos = todos.getAll()
        }

        btnMain.setOnClickListener { launchAddTodoScreen() }

        frag = TodoFilterFragment()
        //frag.setFilter(Filter.ALL)

        //displaying a list of Todos
        //VVV hmm....... now we have 2 references of the same list... maybe not a good idea
        displayTodos(todos.getAll())


        btnAll.background.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("todos", todos.toJson())
    }

    //handles todos coming from AddTodoActivity
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when(resultCode) {
            Activity.RESULT_OK -> {
                when(requestCode) {
                    ADD_TODO_CODE -> {

                        val todoJson = data?.extras?.getString(NewTodoActivity.NEW_TODO_EXTRA)
                        val todo = Gson().fromJson(todoJson, Todo::class.java)
                        Log.d("TODOLOG", todo.toString())

                        //add to do to repositories
                        addTodo(todo)
                        recyclerView.adapter?.notifyItemInserted(todos.getCount() - 1)
                    }

                    EDIT_TODO_CODE -> {
                        val todoJson = data?.extras?.getString(NewTodoActivity.NEW_TODO_EXTRA)
                        val todo = Gson().fromJson(todoJson, Todo::class.java)
                        Log.d("TODOLOG", todo.toString())

                        //edit in repository

                        Log.i("EDITLOG", "In MainActivity, indexToReplace = " +
                                intent.getIntExtra(TAG_INDEX_TO_REPLACE, -999))


                        //todos.replace(intent.getIntExtra(TAG_INDEX_TO_REPLACE, 0), todo)
                        //can't get above to work with passing intents for some reason


                        Log.i("EDITLOG", "In MainActivity, lastIndexToReplace = " + lastIndexToReplace)
                                todos.replace(lastIndexToReplace, todo)
                        recyclerView.adapter?.notifyDataSetChanged()

                        updateDisplay()
                        lastIndexToReplace = -1
                    }
                }
            }
        }
    }

    companion object
    {
        val ADD_TODO_CODE = 1
        val EDIT_TODO_CODE = 2

        val TAG_INDEX_TO_REPLACE = "index"
    }
}

enum class Filter{ALL, ACTIVE, COMPLETED}
