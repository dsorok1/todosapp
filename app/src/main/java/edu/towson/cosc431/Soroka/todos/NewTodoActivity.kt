package edu.towson.cosc431.Soroka.todos

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import edu.towson.cosc431.Soroka.todos.models.Todo
import kotlinx.android.synthetic.main.activity_new_todo.*
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.LocalDateTime
import java.util.*

class NewTodoActivity: AppCompatActivity() {

    private var indexToReplace = -1

    private fun displayContents(title: String, contents: String, checked: Boolean, image: String)
    {
        textTitle.setText(title)
        textContents.setText(contents)
        cbCompleted.isChecked = checked
        //image, so do nothing
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_todo)

        Log.i("EDITLOG", "In onCreate")

        val data = intent

        if(data.hasExtra(MainActivity.TAG_INDEX_TO_REPLACE))
        {//coming from main to edit

            indexToReplace = data.getIntExtra(MainActivity.TAG_INDEX_TO_REPLACE, -1)

            //display
            Log.i("EDITLOG", "Reached editing")
            displayContents(
                data.getStringExtra(TAG_TITLE),
                data.getStringExtra(TAG_CONTENTS),
                data.getBooleanExtra(TAG_COMPLETED, false),
                data.getStringExtra(TAG_IMAGE))
        }

        if(savedInstanceState != null)
        {
            //if coming from a rotation

            textTitle.setText(savedInstanceState.getString(TAG_TITLE))
            textContents.setText(savedInstanceState.getString(TAG_CONTENTS))
            cbCompleted.isChecked = (savedInstanceState.getBoolean(TAG_COMPLETED))
            //same thing for image (not implemented)
            //textImage.setText(savedInstanceState.getBoolean(TAG_IMAGE))
        }

        btnSave.setOnClickListener{btnSaveClicked()}
    }

    //called when screen is rotated
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        //saves typed in content
        outState.putString(TAG_TITLE, ""+textTitle.text)
        outState.putString(TAG_CONTENTS, ""+textContents.text)
        outState.putBoolean(TAG_COMPLETED, cbCompleted.isChecked)
        outState.putString(TAG_IMAGE, "")
    }

    private fun isValid() : Boolean
    {
        return textTitle.text.isNotEmpty() && textContents.text.isNotEmpty()
    }

    private fun makeTodoGuaranteedValid() : Todo{
        val date = SimpleDateFormat("dd/M/yyyy hh:mm:ss").format(Date())
        return Todo(UUID.randomUUID().toString(), textTitle.text.toString(),
            textContents.text.toString(),
            cbCompleted.isChecked,
            "",
            date)
    }

    private fun launchMainScreen() {

        val todo = makeTodoGuaranteedValid()

        val inte = Intent()

        if(indexToReplace != -1)
        {//then it must be editing something
            Log.i("EDITLOG", "In launchMainScreen, indexToReplace = " + indexToReplace)
            inte.putExtra(MainActivity.TAG_INDEX_TO_REPLACE, indexToReplace)
        }

        inte.putExtra(NEW_TODO_EXTRA, todo.toJson())

        setResult(Activity.RESULT_OK, inte)
        finish()//this starts the old activity again? I think...
    }

    //called when the save button is clicked
    private fun btnSaveClicked()
    {
        if(isValid())
        {//if the input is valid, launch main activity
            launchMainScreen()
        }
        else
        {
            Toast.makeText(this, "Please complete all fields.", Toast.LENGTH_SHORT).show()
        }
    }

    companion object {
        val NEW_TODO_EXTRA = "NEW_TODO_EXTRA"


        val TAG_TITLE = "title"
        val TAG_CONTENTS = "contents"
        val TAG_COMPLETED = "completed"
        val TAG_IMAGE = "image"
    }
}
