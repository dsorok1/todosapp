package edu.towson.cosc431.Soroka.todos.models

import com.google.gson.Gson
import edu.towson.cosc431.Soroka.todos.TodoDatabase
import edu.towson.cosc431.Soroka.todos.interfaces.ITodoRepository
import java.lang.Exception
import java.text.SimpleDateFormat
import java.util.*

class TodoRepository(db: TodoDatabase): ITodoRepository {

    private var todos: MutableList<Todo> = mutableListOf()

    init{
        //load from database
        todos.addAll(db.getTodos())
    }
/*
    private fun defaultSeed() {
        //whoa cool nice anonymous functions
        val init = (0..10).map { Todo("Todo"+it, "Contents"+it,
            (it%2)==1, "", SimpleDateFormat("dd/M/yyyy hh:mm:ss").format(Date()))}
        todos.addAll(init)
    }*/

    //Uses Gson to serialize the song into json
    override fun toJson(): String {
        return Gson().toJson(this)
    }

    override fun getCount(): Int {
        return todos.size
    }

    override fun getTodo(idx: Int): Todo {
        return todos[idx]
    }

    override fun getAll(): List<Todo> {
        return todos
    }

    override fun remove(todo: Todo) {
        todos.remove(todo)
    }

    override fun replace(idx: Int, todo: Todo) {
        if(idx >= todos.size) throw Exception("Outside of bounds")
        todos[idx] = todo
    }

    override fun addTodo(todo: Todo) {
        todos.add(todo)
    }

    override fun toggleCompleted(idx: Int){
        todos[idx].toggleCompleted()
    }

    override fun getIndexOf(todo : Todo): Int
    {
        return todos.indexOf(todo)
    }
}