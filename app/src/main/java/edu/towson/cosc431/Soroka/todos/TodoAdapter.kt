package edu.towson.cosc431.Soroka.todos

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.app.DialogFragment
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.towson.cosc431.Soroka.todos.interfaces.ITodoController
import edu.towson.cosc431.Soroka.todos.models.Todo
import kotlinx.android.synthetic.main.activity_new_todo.view.*
import kotlinx.android.synthetic.main.todo_view.view.*

class TodoAdapter (val todos: List<Todo>, val controller: ITodoController, val context : Context): RecyclerView.Adapter<TodoViewHolder>(){
    @SuppressLint("NewApi")
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TodoViewHolder {
        /*
        1. inflate view
        2. create viewholder
        3. *setup*
        4. return viewholder
        */
        val view = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.todo_view, parent, false)

        val holder = TodoViewHolder(view)

        //edit todo
        holder.itemView.setOnClickListener{
            //launch newTodoActicity with stuff filled out already, edit on return
            //val intent = Intent(context, NewTodoActivity::class.java)
            //startActivity(this@MainActivity, MainActivity.EDIT_TODO_CODE)

            //val result = reallyDeleteDialog()
            val position = holder.adapterPosition
            controller.editTodo(position)
            notifyItemChanged(position)
        }

        //give confirmation window and delete
        holder.itemView.setOnLongClickListener{
            reallyDeleteDialog(holder)
            true
        }

        //checkbox updates todos
        holder.itemView.cbTodoCompleted.setOnClickListener{
            val position = holder.adapterPosition
            controller.toggleCompleted(position)
            //notifyDataSetChanged()
        }

        return holder
    }

    private fun deleteTodo(holder: TodoViewHolder) {
        val position = holder.adapterPosition
        controller.deleteTodo(position)
        notifyItemRemoved(position)
    }

    override fun getItemCount(): Int {
        return todos.size
    }

    override fun onBindViewHolder(holder: TodoViewHolder, position: Int) {
        //2 steps
        //1. get todo out of list
        //2. set properties on viewholder
        val todo = todos.get(position)
        holder.itemView.textTodoTitle.text = todo.title//grabbing view that we passed it, set properties on view
        holder.itemView.textTodoContents.text = todo.contents
        holder.itemView.textDateCreated.text = todo.dateCreated
        holder.itemView.cbTodoCompleted.isChecked = todo.isCompleted
    }
    //pass a list of songs in constructer and extend recycler view


    //creates a dialog
    @RequiresApi(Build.VERSION_CODES.M)
    fun reallyDeleteDialog(holder: TodoViewHolder){
        // Use the Builder class for convenient dialog construction
        val builder = AlertDialog.Builder(context)
        builder.setMessage(R.string.really_delete)
            .setPositiveButton(R.string.yes,
                DialogInterface.OnClickListener { dialog, id ->
                    deleteTodo(holder)
                })
            .setNegativeButton(R.string.cancel,
                DialogInterface.OnClickListener { dialog, id ->
                    // User cancelled the dialog
                })
        // Create the AlertDialog object and return it
        val dialog = builder.create()
        dialog.show()
    }
}

class TodoViewHolder(view: View) : RecyclerView.ViewHolder(view)