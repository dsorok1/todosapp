package edu.towson.cosc431.Soroka.todos.fragments

import android.content.Context
import android.graphics.PorterDuff
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat.getColor
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import edu.towson.cosc431.Soroka.todos.R
import android.util.Log
import edu.towson.cosc431.Soroka.todos.Filter
import edu.towson.cosc431.Soroka.todos.MainActivity
import edu.towson.cosc431.Soroka.todos.TodoAdapter
import edu.towson.cosc431.Soroka.todos.interfaces.ITodoController
import edu.towson.cosc431.Soroka.todos.interfaces.ITodoRepository
import edu.towson.cosc431.Soroka.todos.models.Todo
import edu.towson.cosc431.Soroka.todos.models.TodoRepository
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.todo_filter_fragment.*
import kotlinx.android.synthetic.main.todo_filter_fragment.view.*

//TODO: this fragment and the main activity seem pretty heavily coupled. however it is 2 am and i don't care anymore. it works.

class TodoFilterFragment : Fragment() {

    lateinit var v: View
    lateinit var filteredTodos: List<Todo>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.todo_filter_fragment, container, false)

        v.btnAll.setOnClickListener()
        {
            btnAll.background.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)
            btnActive.background.clearColorFilter()
            btnCompleted.background.clearColorFilter()

            (activity as MainActivity).updateCurrentFilter(Filter.ALL)
            (activity as MainActivity).updateDisplay()
            //filteredTodos = setFilter(Filter.ALL)
            //(activity as MainActivity).displayTodos(filteredTodos)
        }

        v.btnActive.setOnClickListener()
        {
            btnAll.background.clearColorFilter()
            btnActive.background.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)
            btnCompleted.background.clearColorFilter()

            (activity as MainActivity).updateCurrentFilter(Filter.ACTIVE)
            (activity as MainActivity).updateDisplay()
        }

        v.btnCompleted.setOnClickListener()
        {
            btnAll.background.clearColorFilter()
            btnActive.background.clearColorFilter()
            btnCompleted.background.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)
            (activity as MainActivity).updateCurrentFilter(Filter.COMPLETED)
            (activity as MainActivity).updateDisplay()
        }

        //CAN'T DO THIS! it seems like the button doesn't exist yet
        //btnAll.background.setColorFilter(resources.getColor(R.color.colorPrimary), PorterDuff.Mode.MULTIPLY)

        return v
    }
}
