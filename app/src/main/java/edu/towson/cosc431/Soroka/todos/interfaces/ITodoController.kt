package edu.towson.cosc431.Soroka.todos.interfaces

import edu.towson.cosc431.Soroka.todos.models.Todo

interface ITodoController {
        fun deleteTodo(todoIndex: Int)
        fun toggleCompleted(todoIndex: Int)
        fun addTodo(todo : Todo)
        fun editTodo(todoIndex: Int)
        fun launchAddTodoScreen()
}