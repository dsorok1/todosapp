package edu.towson.cosc431.Soroka.todos

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.provider.BaseColumns
import edu.towson.cosc431.Soroka.todos.models.Todo

object TodoContract {
    object TodoEntry : BaseColumns {
        const val TABLE_NAME = "todos"
        const val COLUMN_NAME_TITLE = "title"
        const val COLUMN_NAME_CONTENTS = "contents"
        const val COLUMN_NAME_COMPLETED = "completed"
        const val COLUMN_NAME_IMAGE = "image"
        const val COLUMN_NAME_DATE = "date"
        const val COLUMN_NAME_DELETED = "deleted"
    }
}

interface IDatabase {
    fun addTodo(t: Todo)
    fun getTodos(): List<Todo>
    //fun getTodo(id: String): Todo
    fun updateTodo(f: Todo)
    fun deleteTodo(t: Todo)
}
/*
var title : String
var contents : String,
var isCompleted : Boolean
var image : String,
var dateCreated : String
 */

//best way to delete is to not delete
//boolean flag marked as deleted
private const val SQL_CREATE_ENTRIES =
    "CREATE TABLE ${TodoContract.TodoEntry.TABLE_NAME} (" +
            "${BaseColumns._ID} TEXT PRIMARY KEY," +
            "${TodoContract.TodoEntry.COLUMN_NAME_TITLE} TEXT," +
            "${TodoContract.TodoEntry.COLUMN_NAME_CONTENTS} TEXT," +
            "${TodoContract.TodoEntry.COLUMN_NAME_COMPLETED} INTEGER DEFAULT 0," + //DEFAULT is IMPORTANT!!!!!!
            "${TodoContract.TodoEntry.COLUMN_NAME_IMAGE} TEXT," +
            "${TodoContract.TodoEntry.COLUMN_NAME_DATE} TEXT," +
            "${TodoContract.TodoEntry.COLUMN_NAME_DELETED} INTEGER DEFAULT 0)"

private const val SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS ${TodoContract.TodoEntry.TABLE_NAME}"

class TodoDatabase(ctx: Context) : IDatabase {
    override fun deleteTodo(t: Todo) {
        val cvs = ContentValues()
        cvs.put(TodoContract.TodoEntry.COLUMN_NAME_DELETED, "1")
        db.update(
            TodoContract.TodoEntry.TABLE_NAME,
            cvs,
            "${BaseColumns._ID} = ?",   // where clause
            arrayOf(t.id)// where args
        )
    }

    override fun addTodo(p: Todo) {
        val values = toContentValues(p)
        db.insert(TodoContract.TodoEntry.TABLE_NAME, null, values)
    }

    /*override fun getTodo(id: String):Todo
    {
        return "oops"
    }*/

    override fun updateTodo(f: Todo) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getTodos(): List<Todo> {
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        val projection = arrayOf(BaseColumns._ID, TodoContract.TodoEntry.COLUMN_NAME_TITLE,
            TodoContract.TodoEntry.COLUMN_NAME_CONTENTS, TodoContract.TodoEntry.COLUMN_NAME_COMPLETED,
            TodoContract.TodoEntry.COLUMN_NAME_IMAGE, TodoContract.TodoEntry.COLUMN_NAME_DATE)

        // How you want the results sorted in the resulting Cursor
        val sortOrder = "${BaseColumns._ID} DESC"
        val cursor = db.query(
            TodoContract.TodoEntry.TABLE_NAME,
            projection,
            "${TodoContract.TodoEntry.COLUMN_NAME_DELETED} < ?",
            arrayOf("0"),
            null, // group
            null, // groupBy
            sortOrder
        )
        val result = mutableListOf<Todo>()
        with(cursor) {
            while(cursor.moveToNext()) {
                val id = getString(getColumnIndex(BaseColumns._ID))
                val title = getString(getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_TITLE))
                val contents = getString(getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_CONTENTS))
                val isCompletedI = getInt(getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_COMPLETED))
                var isCompleted = false
                if(isCompletedI == 1)
                    isCompleted = true
                val image = getString(getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_IMAGE))
                val date = getString(getColumnIndex(TodoContract.TodoEntry.COLUMN_NAME_DATE))
                result.add(Todo(id, title, contents, isCompleted, image, date))
            }
        }
        return result
    }

    /*
    (var id: String, var title : String, var contents : String,
                        var isCompleted : Boolean, var image : String,
                        var dateCreated : String)
     */

    private fun toContentValues(t: Todo): ContentValues {
        val cv = ContentValues()
        cv.put(BaseColumns._ID, t.id)
        cv.put(TodoContract.TodoEntry.COLUMN_NAME_TITLE, t.title)
        cv.put(TodoContract.TodoEntry.COLUMN_NAME_CONTENTS, t.contents)
        cv.put(TodoContract.TodoEntry.COLUMN_NAME_COMPLETED, t.isCompleted)
        cv.put(TodoContract.TodoEntry.COLUMN_NAME_IMAGE, t.image)
        cv.put(TodoContract.TodoEntry.COLUMN_NAME_DATE, t.dateCreated)
        return cv
    }

    private val db: SQLiteDatabase

    init {
        db = TodoDbHelper(ctx).writableDatabase
    }

    class TodoDbHelper(ctx: Context) : SQLiteOpenHelper(ctx, DATABASE_NAME, null, DATABASE_VERSION)
    {
        override fun onCreate(db: SQLiteDatabase?) {
            db?.execSQL(SQL_CREATE_ENTRIES)
        }

        override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
            if(newVersion == 2)
            {
                db?.execSQL("ALTER_TABLE ${TodoContract.TodoEntry.TABLE_NAME} " +
                        "ADD COLUMN ${TodoContract.TodoEntry.COLUMN_NAME_DELETED} " +
                        "DEFAULT 0")
            }
            else {
                db?.execSQL(SQL_DELETE_ENTRIES)
                onCreate(db)
            }
        }

        companion object {
            val DATABASE_NAME = "Todo.db"
            val DATABASE_VERSION = 2
        }
    }
}

